//
//  SearchByServiceViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/15/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SearchByServiceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIScrollViewDelegate {
    
    

    var totaldoorstep = 0
    @IBOutlet weak var noResultFoundView: UIView!
    var found = 0
    var tagnum = 0
    //MARK : Services views on scroll views...
    @IBOutlet weak var serviceOne_OnScrollOne: UIView!
    var selectedServiceCounter = 0
    var data = ["Loreal", "Lorealc", "Loreal", "Delhi", "Chandigarh", "Punchkula"]
    var buisenessId = [Int]()
    var filtered:[String] = []
    var searchString = [String]()
    var oldData = ["Loreal", "Lorealc", "Loreal", "Delhi", "Chandigarh", "Punchkula"]
    var searchCounter = 0
    var userSelection = String()
    var imageCache = [String : UIImage]()
    var imageUrls = [String]()
    var imageArray = [UIImage]()
    var distance = [15, 5, 2, 3, 7]
    var address = ["Delhi", "Chandigarh", "Punchkula", "Mohali", "Ambala"]
    var dictionary = [String: Array<String>]()
    var saveStateDoorStep = [Int]()
    var hairSaveState =  [Int]()
    var faceSaveState = [Int]()
    var handfeetSaveState = [Int]()
    var bridalSaveState = [Int]()
    var spaMassageSaveState = [Int]()
    var menSaveState = [Int]()
    var clinicSaveState = [Int]()
    var moreSaveState = [Int]()
    var rupee = ["60", "70", "80", "90", "100", "1000"]
    var categoryArray = ["DoorStep","HairCare","FaceCare","HandFeet","Bridal","Spa Massage","Men","Clinic","More Srvices"]
    
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var viewThreeHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTwoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchSalonTextField: UITextField!
    
    @IBOutlet weak var topBackgroundImage: UIImageView!
    @IBOutlet weak var byServicesButton: UIButton!
    @IBOutlet weak var bySalonButton: UIButton!
    @IBOutlet weak var locationPointer: UILabel!
    @IBOutlet weak var searchByNamePointer: UILabel!
    @IBOutlet weak var shoppingCartButton: UIButton!
    @IBOutlet weak var numberOfSelectedServicesLabel: UILabel!
    @IBOutlet weak var foundServicesView: UIView!
    @IBOutlet weak var salonTableView: UITableView!
    @IBOutlet weak var verticalScrollView: UIScrollView!
    @IBOutlet weak var servicesFoundLabel: UILabel!
    
    @IBOutlet weak var servicesHorizontalScrollOne: UIScrollView!
    //Show services scroll view...
   //@IBOutlet weak var servicesHorizontalScrollOne: UIScrollView!
    @IBOutlet weak var servicesHorizontalScrollTwo: UIScrollView!
    @IBOutlet weak var servicesHorizontalScrollThree: UIScrollView!
    
    @IBOutlet weak var searchServicesView: UIView!
    @IBOutlet weak var rupeeLabel: UILabel!
    
    //MARK: -Imagebox and Labes outlets on Services buttons...
    @IBOutlet weak var doorstepImageBox: UIImageView!
    @IBOutlet weak var doorLabel: UILabel!
    @IBOutlet weak var hairImagBox: UIImageView!
    @IBOutlet weak var hairLabel: UILabel!
    @IBOutlet weak var faceCareImageBox: UIImageView!
    @IBOutlet weak var facecareLabel: UILabel!
    @IBOutlet weak var hand_feetImageBox: UIImageView!
    @IBOutlet weak var handfeetLabel: UILabel!
    @IBOutlet weak var bridalImageBox: UIImageView!
    @IBOutlet weak var bridalLabel: UILabel!
    @IBOutlet weak var spaMassageImageBox: UIImageView!
    @IBOutlet weak var spaMassageLabel: UILabel!
    @IBOutlet weak var menImageBox: UIImageView!
    @IBOutlet weak var menSalonLabel: UILabel!
    @IBOutlet weak var clinicImageBox: UIImageView!
    @IBOutlet weak var clinicLabel: UILabel!
    @IBOutlet weak var moreImageBox: UIImageView!
    @IBOutlet weak var moreLabel: UILabel!
    
    //MARK: - Services buttons outlets...
    @IBOutlet weak var doorStepButton: UIButton!
    @IBOutlet weak var hairButton: UIButton!
    @IBOutlet weak var faceCareButton: UIButton!
    @IBOutlet weak var handsfeetButton: UIButton!
    @IBOutlet weak var bridalButton: UIButton!
    @IBOutlet weak var spa_massageButton: UIButton!
    @IBOutlet weak var men_salonButton: UIButton!
    @IBOutlet weak var clinicButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
   
    // create view and position it offscreen to the left
    let bookedServicesView = UIView()
    let sideMenuView = UIView()
    let width:CGFloat = 350
    let height:CGFloat = 647
    let yPosition:CGFloat = 40
    var  slideServicesTable = UITableView(frame: CGRectMake(0, 60, 275, 600))
    var  slideMenuTable = UITableView(frame: CGRectMake(0, 60, 275, 600))
    var categoryLabel: UILabel = UILabel()
    
    @IBAction func slideMenuButton(sender: UIButton) {
        var sideBookedButton   = UIButton.buttonWithType(UIButtonType.System) as UIButton
        sideBookedButton.frame = CGRectMake(25, 13, 40, 40)
        sideBookedButton.setBackgroundImage(UIImage(named: "back_button@3x.png"), forState: UIControlState.Normal)
        sideBookedButton.setTitle("Button", forState: UIControlState.Normal)
        var tapRecognizer = UITapGestureRecognizer(target: self, action: "hideMenuPane")
        sideBookedButton.addGestureRecognizer(tapRecognizer)
        self.sideMenuView.addSubview(sideBookedButton)

        UIView.animateWithDuration(0.3, animations: {
            
            self.sideMenuView.frame = CGRectMake( 0, self.yPosition, 275, self.height)
        })
        UIView.animateWithDuration(0.4, animations: {
            
            self.bookedServicesView.frame = CGRectMake(400, self.yPosition, self.width, self.height)
        })
        

    }
    func hideMenuPane(){
        UIView.animateWithDuration(0.4, animations: {
            
            self.sideMenuView.frame = CGRectMake(-self.width, self.yPosition, 275, self.height)
        })
         //self.view.alpha = 1
    }

    
    @IBAction func bookedServicesButton(sender: UIButton) {
      
            var sideBookedButton   = UIButton.buttonWithType(UIButtonType.System) as UIButton
            sideBookedButton.frame = CGRectMake(25, 13, 40, 40)
            sideBookedButton.setBackgroundImage(UIImage(named: "Shopingcart@3x.png"), forState: UIControlState.Normal)
            sideBookedButton.setTitle("Button", forState: UIControlState.Normal)
            var tapRecognizer = UITapGestureRecognizer(target: self, action: "hideBookPane")
            sideBookedButton.addGestureRecognizer(tapRecognizer)
            self.bookedServicesView.addSubview(sideBookedButton)
        
            var label: UILabel = UILabel()
            label.frame = CGRectMake(60, 20, 200, 21)
            label.backgroundColor = UIColor.clearColor()
            label.textColor = UIColor.grayColor()
            label.font = UIFont(name: "Raleway-Medium.ttf", size: 20)
            label.textAlignment = NSTextAlignment.Center
            label.text = "Services Selected"
            self.bookedServicesView.addSubview(label)
           //hie the slide menu...
            UIView.animateWithDuration(0.3, animations: {
                
                self.bookedServicesView.frame = CGRectMake(100, self.yPosition, self.width, self.height)
            })
            UIView.animateWithDuration(0.4, animations: {
            
                self.sideMenuView.frame = CGRectMake(-self.width, self.yPosition, 275, self.height)
            })
      
    }
    
    func hideBookPane(){
        UIView.animateWithDuration(0.4, animations: {
            
            self.bookedServicesView.frame = CGRectMake(400, self.yPosition, self.width, self.height)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //totalRow()
         slideServicesTable.tag = 5002
         slideServicesTable.delegate = self
         slideServicesTable.dataSource = self
         slideServicesTable.showsVerticalScrollIndicator = true
         slideServicesTable.rowHeight = self.view.frame.size.height / 9
         self.bookedServicesView.addSubview( slideServicesTable)
         slideServicesTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
         slideMenuTable.tag = 5003
         slideMenuTable.scrollEnabled = false
         slideMenuTable.delegate = self
         slideMenuTable.dataSource = self
         slideMenuTable.showsVerticalScrollIndicator = true
         slideMenuTable.rowHeight = self.view.frame.size.height / 11
         slideMenuTable.separatorColor = UIColor.clearColor()
         self.sideMenuView.addSubview(slideMenuTable)
         slideMenuTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
         //side booke services  uiview....
        bookedServicesView.frame = CGRectMake(375, yPosition, width, self.view.frame.size.height)
        self.view.bringSubviewToFront(bookedServicesView)
        self.view.addSubview(bookedServicesView)
        bookedServicesView.backgroundColor = UIColor.whiteColor()
        saveStateDoorStep =  Array(count: oldData.count, repeatedValue: 0)
        
        //side menu uiview....
        sideMenuView.frame = CGRectMake(-width, yPosition, 275, self.view.frame.size.height)
        self.view.bringSubviewToFront(sideMenuView)
        self.view.addSubview(sideMenuView)
        sideMenuView.backgroundColor = UIColor.whiteColor()
        saveStateDoorStep =  Array(count: oldData.count, repeatedValue: 0)
        
        hairSaveState =  Array(count: rupee.count, repeatedValue: 0)
        faceSaveState = Array(count: rupee.count, repeatedValue: 0)
        handfeetSaveState =  Array(count: oldData.count, repeatedValue: 0)
        bridalSaveState =  Array(count: rupee.count, repeatedValue: 0)
        spaMassageSaveState =  Array(count: rupee.count, repeatedValue: 0)
        menSaveState = Array(count: oldData.count, repeatedValue: 0)
        clinicSaveState =  Array(count: rupee.count, repeatedValue: 0)
        moreSaveState =  Array(count: rupee.count, repeatedValue: 0)
        
        dictionary["search_key"] = oldData
        
        doorStepButton.addTarget(self, action: "pressedDoorStep:", forControlEvents: UIControlEvents.TouchUpInside)
        hairButton.addTarget(self, action: "pressedHairButton:", forControlEvents: UIControlEvents.TouchUpInside)
        faceCareButton.addTarget(self, action: "pressedFaceCareButton:", forControlEvents: UIControlEvents.TouchUpInside)
        handsfeetButton.addTarget(self, action: "pressedHandSFeetButton:", forControlEvents: UIControlEvents.TouchUpInside)
        bridalButton.addTarget(self, action: "pressedBridalButton:", forControlEvents: UIControlEvents.TouchUpInside)
        spa_massageButton.addTarget(self, action: "pressedSpa_massageButton:", forControlEvents: UIControlEvents.TouchUpInside)
        men_salonButton.addTarget(self, action: "pressedMenSalonButton:", forControlEvents: UIControlEvents.TouchUpInside)
        clinicButton.addTarget(self, action: "pressedClinicButton:", forControlEvents: UIControlEvents.TouchUpInside)
        moreButton.addTarget(self, action: "pressedMoreButton:", forControlEvents: UIControlEvents.TouchUpInside)
        
        numberOfSelectedServicesLabel.text = "\(selectedServiceCounter)"
        
        servicesHorizontalScrollOne.contentSize = CGSizeMake(0   , 66)
        servicesHorizontalScrollTwo.contentSize = CGSizeMake(0   , 66)
        servicesHorizontalScrollThree.contentSize = CGSizeMake(0   , 66)
        
        heightView.constant = 110
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 110
        
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = true
        
        shoppingCartButton.hidden = false
        numberOfSelectedServicesLabel.hidden = false
        verticalScrollView.hidden = false
        
        numberOfSelectedServicesLabel.layer.cornerRadius = numberOfSelectedServicesLabel.frame.size.width/2
        numberOfSelectedServicesLabel.layer.masksToBounds = true
        
        locationPointer.layer.cornerRadius = locationPointer.frame.size.width/2
        locationPointer.layer.masksToBounds = true
        
        searchByNamePointer.layer.cornerRadius = searchByNamePointer.frame.size.width/2
        searchByNamePointer.layer.masksToBounds = true
        searchServicesView.hidden = true
        
        byServicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        self.salonTableView.rowHeight = 130
        
        var hideKeyboardRecognizer = UITapGestureRecognizer(target: self, action: "closeKeyBoard")
        //self.headerView.addGestureRecognizer(tapRecognizer)
        self.view.addGestureRecognizer(hideKeyboardRecognizer)

        var tapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewTapped")
        bookedServicesView.addGestureRecognizer(tapRecognizer)
    }

    func closeKeyBoard() {
        self.view.endEditing(true)
       
    }
    func scrollViewTapped() {
        UIView.animateWithDuration(0.4, animations: {
            
            self.bookedServicesView.frame = CGRectMake(400, self.yPosition, self.width, self.height)
        })
    }
    
    @IBAction func byServicesButton(sender: AnyObject) {
        noResultFoundView.hidden = true
        verticalScrollView.hidden = false
        shoppingCartButton.hidden = false
        numberOfSelectedServicesLabel.hidden = false
        foundServicesView.hidden = true
        salonTableView.hidden = true
        searchServicesView.hidden = true
        byServicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        bySalonButton.backgroundColor = UIColor.clearColor()
        //bySalonButton.alpha = 0.1
    }
    @IBAction func bySalonButton(sender: UIButton) {
         //bySalonButton.alpha = 1
        noResultFoundView.hidden = false
        verticalScrollView.hidden = true
        foundServicesView.hidden = false
        shoppingCartButton.hidden = true
        numberOfSelectedServicesLabel.hidden = true
        searchServicesView.hidden = false
        bySalonButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        byServicesButton.backgroundColor = UIColor.clearColor()
    }
    
     //MARK: - Services buttons...
    @IBAction func doorStepButton(sender: UIButton) {
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedDoorStep(sender: UIButton){
         println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            doorstepImageBox.image = UIImage(named: "door@3x.png")
            doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            doorLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            doorstepImageBox.image = UIImage(named: "doorpink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = false
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 178
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            sender.selected = true
            doorStepServices()
        }
    }
    
    @IBAction func hairButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedHairButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
            hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            hairLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            hairImagBox.image = UIImage(named: "haircolorpink 2@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = false
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 178
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            sender.selected = true
            hairCareServices()
        }
    }
    
    @IBAction func faceCareButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedFaceCareButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
           facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            facecareLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            faceCareImageBox.image = UIImage(named: "facial 2pink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = false
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 178
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            sender.selected = true
            faceCareServices()
        }
    }

    
    @IBAction func handsfeetButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)

    }
    func pressedHandSFeetButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
            handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            handfeetLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            hand_feetImageBox.image = UIImage(named: "hands_feetpink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = false
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 178
            viewThreeHeight.constant = 110
            sender.selected = true
            handsFeetservices()
        }
    }
    
    @IBAction func bridalButton(sender: UIButton) {
        
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedBridalButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            bridalImageBox.image = UIImage(named: "bridal.png")
            bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            bridalLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
             bridalImageBox.image = UIImage(named: "bridalpink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = false
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 178
            viewThreeHeight.constant = 110
            sender.selected = true
            bridalServices()
        }
    }
    
    @IBAction func spa_massageButton(sender: UIButton) {
        
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedSpa_massageButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
            spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            spaMassageLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            spaMassageImageBox.image = UIImage(named: "massage 2pink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = false
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 178
            viewThreeHeight.constant = 110
            sender.selected = true
            spaMassageServices()
        }
    }

    
    @IBAction func men_salonButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedMenSalonButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            menImageBox.image = UIImage(named: "mens salons gray.png")
            menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
           menSalonLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
             menImageBox.image = UIImage(named: "mens salonspink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = false
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 178
            menServices()
            sender.selected = true
        }
    }
    
    @IBAction func clinicButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
        moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    func pressedClinicButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
            clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            clinicLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            clinicImageBox.image = UIImage(named: "clinicspink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = false
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 178
            clinicServices()
            sender.selected = true
        }
    }
    
    @IBAction func moreButton(sender: UIButton) {
        doorstepImageBox.image = UIImage(named: "door@3x.png")
        doorLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hairImagBox.image = UIImage(named: "haircolor 2@3x.png")
        hairLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        faceCareImageBox.image = UIImage(named: "facial 2@3x.png")
        facecareLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        hand_feetImageBox.image = UIImage(named: "hands_feet@3x.png")
        handfeetLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        bridalImageBox.image = UIImage(named: "bridal.png")
        bridalLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        spaMassageImageBox.image = UIImage(named: "massage 2@3x.png")
        spaMassageLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        menImageBox.image = UIImage(named: "mens salons gray.png")
        menSalonLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        clinicImageBox.image = UIImage(named: "clinicsgray@3x.png")
        clinicLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    
    }
    func pressedMoreButton(sender: UIButton){
        println(sender.selected)
        if sender.selected
        {
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = true
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 110
            moreImageBox.image = UIImage(named: "clinicsgray@3x.png")
            moreLabel.textColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
            println("Working")
            sender.selected = false
        }
        else{
            moreLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            moreImageBox.image = UIImage(named: "morepink@3x.png")
            println("Shamim Khan")
            servicesHorizontalScrollOne.hidden = true
            servicesHorizontalScrollTwo.hidden = true
            servicesHorizontalScrollThree.hidden = false
            heightView.constant = 110
            viewTwoHeight.constant = 110
            viewThreeHeight.constant = 178
            moreServices()
            sender.selected = true
        }
    }
    
    func serviceCounter(){
        selectedServiceCounter++
        numberOfSelectedServicesLabel.text = "\(selectedServiceCounter)"
    }
    
    
    var selectedService = [String]()
    var firstNameCell: UITableViewCell = UITableViewCell()
    var shareCell: UITableViewCell = UITableViewCell()
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView.tag == 5002{
            return categoryArray.count
            
        }
        else if  tableView.tag == 5003{
            return 1
        }else{
            return 2
        }
    }
    
    // Customize the section headings for each section
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if tableView.tag == 5002{
            return categoryArray[section]
        }
        else if  tableView.tag == 5003{
            return ""
        }
        else{
            return ""
            
        }
        
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 5002{
          
            return  selectedServiceCounter
            
        }
        else if  tableView.tag == 5003{
            return 9
        }
        else
        {
            return filtered.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.tag == 5001{
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as ServiceTableViewCell
            cell.salonNameCellLabel.text = filtered[indexPath.row]
               return cell
        }
        else if  tableView.tag == 5003{
            var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell

            if indexPath.row == 0{
                let profileImage:UIImageView = UIImageView()
                profileImage.frame = CGRectMake(25, 10, 40, 40)
                profileImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(profileImage)
                
                var profileLabel: UILabel = UILabel()
                profileLabel.frame = CGRectMake(80, 15, 200, 30)
                profileLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                profileLabel.textAlignment = NSTextAlignment.Left
                profileLabel.text = "User Profile"
                profileLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(profileLabel)
            }
            else if indexPath.row == 1{
                let appointmentImage:UIImageView = UIImageView()
                appointmentImage.frame = CGRectMake(25, 10, 40, 40)
                appointmentImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(appointmentImage)
                
                var appointmentLabel: UILabel = UILabel()
                appointmentLabel.frame = CGRectMake(80, 15, 200, 30)
                appointmentLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                appointmentLabel.textAlignment = NSTextAlignment.Left
                appointmentLabel.text = "Book Appointments"
                appointmentLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(appointmentLabel)
            }
            else if indexPath.row == 2{
                let myAppointmentImage:UIImageView = UIImageView()
                myAppointmentImage.frame = CGRectMake(25, 10, 40, 40)
                myAppointmentImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(myAppointmentImage)
                
                var myAppointmentLabel: UILabel = UILabel()
                myAppointmentLabel.frame = CGRectMake(80, 15, 200, 30)
                myAppointmentLabel.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                myAppointmentLabel.textAlignment = NSTextAlignment.Left
                myAppointmentLabel.text = "My Appointments"
                myAppointmentLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(myAppointmentLabel)
            }
            else if indexPath.row == 3{
                let notificationsImage:UIImageView = UIImageView()
                notificationsImage.frame = CGRectMake(25, 10, 40, 40)
                notificationsImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(notificationsImage)
                
                var notificationsLabel: UILabel = UILabel()
                notificationsLabel.frame = CGRectMake(80, 15, 200, 30)
                notificationsLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                notificationsLabel.textAlignment = NSTextAlignment.Left
                notificationsLabel.text = "Notifications"
                notificationsLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(notificationsLabel)
            }
            else if indexPath.row == 4{
                let paymentOptionImage:UIImageView = UIImageView()
                paymentOptionImage.frame = CGRectMake(25, 10, 40, 40)
                paymentOptionImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(paymentOptionImage)
                
                var paymentOptionLabel: UILabel = UILabel()
                paymentOptionLabel.frame = CGRectMake(80, 15, 200, 30)
                paymentOptionLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                paymentOptionLabel.textAlignment = NSTextAlignment.Left
                paymentOptionLabel.text = "Payment Options"
                paymentOptionLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(paymentOptionLabel)
            }
            else if indexPath.row == 5{
                let rateVyomoImage:UIImageView = UIImageView()
                rateVyomoImage.frame = CGRectMake(25, 10, 40, 40)
                rateVyomoImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(rateVyomoImage)
                
                var rateVyomoLabel: UILabel = UILabel()
                rateVyomoLabel.frame = CGRectMake(80, 15, 200, 30)
                rateVyomoLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                rateVyomoLabel.textAlignment = NSTextAlignment.Left
                rateVyomoLabel.text = "Rate Vyomo"
                rateVyomoLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(rateVyomoLabel)
            }
            else if indexPath.row == 6{
                let emailSupportImage:UIImageView = UIImageView()
                emailSupportImage.frame = CGRectMake(25, 10, 40, 40)
                emailSupportImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(emailSupportImage)
                
                var emailSupportLabel: UILabel = UILabel()
                emailSupportLabel.frame = CGRectMake(80, 15, 200, 30)
                emailSupportLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                emailSupportLabel.textAlignment = NSTextAlignment.Left
                emailSupportLabel.text = "Email Support"
                emailSupportLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(emailSupportLabel)
            }
            else if indexPath.row == 7{
                let addBussinessImage:UIImageView = UIImageView()
                addBussinessImage.frame = CGRectMake(25, 10, 40, 40)
                addBussinessImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(addBussinessImage)
                
                var addBussinessLabel: UILabel = UILabel()
                addBussinessLabel.frame = CGRectMake(80, 15, 200, 30)
                addBussinessLabel.textColor =  UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                addBussinessLabel.textAlignment = NSTextAlignment.Left
                addBussinessLabel.text = "Add Your Bussiness"
                addBussinessLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(addBussinessLabel)
            }
            else if indexPath.row == 8{
                let profileImage:UIImageView = UIImageView()
                profileImage.frame = CGRectMake(25, 10, 40, 40)
                profileImage.image = UIImage(named: "userplaceholder@3x.png")
                cell.addSubview(profileImage)
                
                var profileLabel: UILabel = UILabel()
                profileLabel.frame = CGRectMake(80, 15, 200, 30)
                profileLabel.textColor = UIColor(red: 50/255, green: 48/255, blue: 49/255, alpha: 1)
                profileLabel.textAlignment = NSTextAlignment.Left
                profileLabel.text = "User Profile"
                profileLabel.font = (UIFont(name: "raleway", size: 18))
                cell.addSubview(profileLabel)
            }
            return cell
        }
        else
        {
            var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
            
            cell.textLabel?.text = selectedService[indexPath.row]
           
            return cell
        }
     
        
    }

    
    
    //MARK : TextField Action
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
            if string != "" {                 //if element is typed not deleted
            searchString.append(string)
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter++
            println(modifiedString)
            println(searchString)
            search(modifiedString)
            salonTableView.hidden = false
            noResultFoundView.hidden = true
        } else {
            searchString.removeLast()
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter--
            println(searchString)
            println(modifiedString)
            search(modifiedString)
            
        }
        println(searchCounter)
        return true
    }

    // MARK: Search Implementation...
    func search(string: String) {
        filtered = oldData.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString("\(string)", options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        if(filtered == []) && searchCounter == 0 {
            data = oldData
        } else {
            data = filtered
        }
        servicesFoundLabel.text = "\(filtered.count) Results Found"
        
        //sort(&filtered)
        println(filtered)
        
        if filtered.count == 0{
            noResultFoundView.hidden = false
        }
        salonTableView.reloadData()
        
    }

   
    // MARK: Services Button Implementation...
    func  doorStepServices(){
        println("door State")
        println(saveStateDoorStep)
        var  starButtonOriginX = servicesHorizontalScrollOne.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(oldData.count)
        for var i = 0; i < oldData.count; i++ {
                var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
                 servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                 servicesButton.tag = 600 + i
                 servicesButton.setTitle(oldData[i], forState: UIControlState.Normal)
                 servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
                 servicesHorizontalScrollOne.addSubview(servicesButton)
                 servicesHorizontalScrollOne.contentSize.width =  starButtonOriginX + width
                 starButtonOriginX = starButtonOriginX + width
                if saveStateDoorStep[i] == 1 {
                    servicesButton.selected = true
                    servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
                }
        }
    }
//    func totalRow()
//    {
//        for one in saveStateDoorStep{
//            if one == 1{
//                totaldoorstep++
//            }
//        }
//        println("totaldoorstep::")
//        println(totaldoorstep)
//    }
    
    func  hairCareServices(){
        println("hair State")
        println(hairSaveState)
        var  starButtonOriginX = servicesHorizontalScrollOne.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(rupee.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 650 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollOne.addSubview(servicesButton)
            servicesHorizontalScrollOne.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if hairSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    func  faceCareServices(){
        println("face State")
        println(faceSaveState)
        var  starButtonOriginX = servicesHorizontalScrollOne.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(rupee.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 700 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollOne.addSubview(servicesButton)
            servicesHorizontalScrollOne.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if faceSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }

  
    func  handsFeetservices(){
         println("HAnd State")
        println(handfeetSaveState)
        var  starButtonOriginX = servicesHorizontalScrollTwo.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(rupee.count)
        for var i = 0; i < oldData.count; i++ {
                    var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
                    servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                    servicesButton.tag = 750 + i
                    servicesButton.setTitle(oldData[i], forState: UIControlState.Normal)
                    servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
                    servicesHorizontalScrollTwo.addSubview(servicesButton)
                    servicesHorizontalScrollTwo.contentSize.width =  starButtonOriginX + width
                    starButtonOriginX = starButtonOriginX + width
            if handfeetSaveState[i] == 1 {
                servicesButton.selected = true
                 servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
                }
    }
    
    func  bridalServices(){
        println("bridal State")
        println(bridalSaveState)
        var  starButtonOriginX = servicesHorizontalScrollTwo.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(rupee.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 800 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollTwo.addSubview(servicesButton)
            servicesHorizontalScrollTwo.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if bridalSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    func  spaMassageServices(){
        println("Spa State")
        println(spaMassageSaveState)
        var  starButtonOriginX = servicesHorizontalScrollTwo.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(rupee.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 850 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollTwo.addSubview(servicesButton)
            servicesHorizontalScrollTwo.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if spaMassageSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    func  menServices(){
        println("men State")
        println(menSaveState)
        var  starButtonOriginX = servicesHorizontalScrollThree.frame.origin.x
        var width: CGFloat = 110
        for var i = 0; i < oldData.count; i++ {
                    var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
                    servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
                    servicesButton.tag = 900 + i
                    servicesButton.setTitle(oldData[i], forState: UIControlState.Normal)
                    servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
                    servicesHorizontalScrollThree.addSubview(servicesButton)
                    servicesHorizontalScrollThree.contentSize.width =  starButtonOriginX + width
                    starButtonOriginX = starButtonOriginX + width
            if menSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    func  clinicServices(){
        println("clinic State")
        println(clinicSaveState)
        var  starButtonOriginX = servicesHorizontalScrollThree.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(oldData.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 950 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollThree.addSubview(servicesButton)
            servicesHorizontalScrollThree.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if clinicSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    
    func  moreServices(){
        println("more State")
        println(moreSaveState)
        var  starButtonOriginX = servicesHorizontalScrollThree.frame.origin.x
        var width: CGFloat = 110
        var n: CGFloat = CGFloat(oldData.count)
        for var i = 0; i < rupee.count; i++ {
            var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            servicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            servicesButton.tag = 1000 + i
            servicesButton.setTitle(rupee[i], forState: UIControlState.Normal)
            servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
            servicesHorizontalScrollThree.addSubview(servicesButton)
            servicesHorizontalScrollThree.contentSize.width =  starButtonOriginX + width
            starButtonOriginX = starButtonOriginX + width
            if moreSaveState[i] == 1 {
                servicesButton.selected = true
                servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            }
        }
    }
    
    func pressed(sender: UIButton){
        var str: String = ""
        if sender.selected {
            if sender.tag < 650{
                saveStateDoorStep[sender.tag - 600] = 0
                let text = sender.titleLabel?.text
                str = text!
            }else if sender.tag < 700 && sender.tag >= 650{
                hairSaveState[sender.tag - 650] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 750 && sender.tag >= 700{
                faceSaveState[sender.tag - 700] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 800 && sender.tag >= 750{
                handfeetSaveState[sender.tag - 750] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 850 && sender.tag >= 800{
                bridalSaveState[sender.tag - 800] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 900 && sender.tag >= 850{
                spaMassageSaveState[sender.tag - 850] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 950 && sender.tag >= 900{
                menSaveState[sender.tag - 900] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 1000 && sender.tag >= 950{
                clinicSaveState[sender.tag - 950] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            else if sender.tag < 1050 && sender.tag >= 1000{
                moreSaveState[sender.tag - 1000] = 0
                let text = sender.titleLabel?.text
                str = text!
            }
            selectedServiceCounter--
            sender.selected = false
            sender.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
            for (var i = 0; i<selectedService.count; i++){
                if str == selectedService[i]{
                    selectedService.removeAtIndex(i)
                }
                
            }
            println("Removed....")
            println(selectedService)
            self.slideServicesTable.reloadData()
            
        }
        else {
            if sender.tag < 650{
                saveStateDoorStep[sender.tag - 600] = 1
            }else if sender.tag < 700 && sender.tag >= 650{
                hairSaveState[sender.tag - 650] = 1
            }
            else if sender.tag < 750 && sender.tag >= 700{
                faceSaveState[sender.tag - 700] = 1
            }
            else if sender.tag < 800 && sender.tag >= 750{
                handfeetSaveState[sender.tag - 750] = 1
            }
            else if sender.tag < 850 && sender.tag >= 800{
                bridalSaveState[sender.tag - 800] = 1
            }
            else if sender.tag < 900 && sender.tag >= 850{
                spaMassageSaveState[sender.tag - 850] = 1
            }
            else if sender.tag < 950 && sender.tag >= 900{
                menSaveState[sender.tag - 900] = 1
            }
            else if sender.tag < 1000 && sender.tag >= 950{
                clinicSaveState[sender.tag - 950] = 1
            }
            else if sender.tag < 1050 && sender.tag >= 1000{
                 moreSaveState[sender.tag - 1000] = 1
            }
            selectedServiceCounter++
            sender.selected = true
            sender.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
            let text = sender.titleLabel?.text
            selectedService.append(text!)
            println("HIIIii....")
            println(selectedService)
            self.slideServicesTable.reloadData()
        }
        numberOfSelectedServicesLabel.text = "\(selectedServiceCounter)"
        println(selectedServiceCounter)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
