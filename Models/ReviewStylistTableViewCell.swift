//
//  ReviewStylistTableViewCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/24/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ReviewStylistTableViewCell: UITableViewCell {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var rupeesCellLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
