//
//  MyAppointmentTableTwoViewCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class MyAppointmentTableTwoViewCell: UITableViewCell {

    @IBOutlet weak var cellImageCompleted: UIImageView!
    @IBOutlet weak var serviceNameCompleted: UILabel!
    @IBOutlet weak var stylistNameCompleted: UILabel!
    @IBOutlet weak var timeCompleted: UILabel!
    @IBOutlet weak var dateCompleted: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
