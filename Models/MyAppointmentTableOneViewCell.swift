//
//  MyAppointmentTableOneViewCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class MyAppointmentTableOneViewCell: UITableViewCell {

    
    @IBOutlet weak var cellImageUpcoming: UIImageView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var stylistNameUpcoming: UILabel!
    @IBOutlet weak var timeUpcomingLabel: UILabel!
    @IBOutlet weak var dateUpcomingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellImageUpcoming.layer.cornerRadius = 13
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
