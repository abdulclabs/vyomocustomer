//
//  TableStylistCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class TableStylistCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var stylistName: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
