//
//  SelectSalonTableViewCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/28/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SelectSalonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var scrollView: UIScrollView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
