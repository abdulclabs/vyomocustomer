//
//  JSON.swift
//  Vyomo
//
//  Created by Click Labs on 3/24/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class JSON {
    
  func session(url: String, parameters: NSDictionary , completion: (result: AnyObject) -> Void)  {
        if IJReachability.isConnectedToNetwork() {
            
            
            let urlAsString = "\(InitialUrl)\(url)"
            
            let convertedUrl = NSURL(string: urlAsString)!
            
            let urlSession = NSURLSession.sharedSession()
            var sessionConfig = NSURLSessionConfiguration()
            sessionConfig.allowsCellularAccess = true
            
            var flag = 0
            var json = NSDictionary()
            var emptyDictionary = NSDictionary()
            var err: NSError?
            var jsonResult = NSDictionary()
            
            
            if parameters != emptyDictionary {
                
                var request = NSMutableURLRequest(URL: convertedUrl, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 60.0)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                //    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.HTTPMethod = "POST"
                
                var postData : NSData = NSJSONSerialization.dataWithJSONObject(parameters, options: NSJSONWritingOptions.allZeros , error: &err) as NSData!
                request.HTTPBody = postData
                
                let jsonQuery = urlSession.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
                    if (error != nil) {
                        println(error.localizedDescription)
                    }
                    
                    jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSDictionary
                    
      
                    flag = jsonResult["status"]! as Int
                    self.flagCheck(flag)
                    println(jsonResult["message"]! as NSString)
                    message = jsonResult["message"]! as NSString
                  completion(result: jsonResult )
                })
                
                jsonQuery.resume()
                
                
            } else {
                
                
                let jsonQuery = urlSession.dataTaskWithURL(convertedUrl, completionHandler: { data, response, error -> Void in
                    if (error != nil) {
                        println(error.localizedDescription)
                    }
                    
                    jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSDictionary
                    
                    if (err != nil) {
                        println("JSON Error \(err!.localizedDescription)")
                    }
                    
                    flag = jsonResult["status"]! as Int
                    self.flagCheck(flag)
                    println(jsonResult["message"]! as NSString)
                    completion(result: jsonResult["data"]!)
                    
                })
                
                jsonQuery.resume()
                
            }
        } else {
            
            
        }
    }

    
    func flagCheck(flag: Int){
        
        if flag == 200 {
            println("success")
        }
        else if flag == 100 {
            println("PARAMETER_MISSING")
        } else if flag == 201 {
            println("SHOW_ERROR_MESSAGE")
        } else if flag == 404 {
            println("ERROR_IN_EXECUTION")
        } else if flag == 204 {
            println("INVALID_ACCESS_TOKEN")
        } else if flag == 400 {
            println("SOMETHING_WENT_WRONG")
        } else if flag == 101 {
            println("INVALID_DEVICE_TYPE")
        } else if flag == 102 {
            println("INVALID_ROLE_ID")
        } else if flag == 103 {
            println("INVALID_EMAIL_ID")
        } else if flag == 104 {
            println("INVALID_BUSINESS_ID")
        } else if flag == 105 {
            println("INVALID_BUSINESS_TYPE_ID")
        } else if flag == 106 {
            println("INVALID_USER_NAME")
        } else if flag == 107 {
            println("PASSWORD_LENGTH_ERROR")
        }
    }
}



