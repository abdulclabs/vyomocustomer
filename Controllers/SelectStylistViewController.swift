//
//  SelectStylistViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SelectStylistViewController: UIViewController , UITextFieldDelegate ,UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchField: UITextField!
    var searchActive : Bool = false
    var data = ["Shamim","Jaskirat","Danish", "Raghav","Pradeep"]
    var buisenessId = [Int]()
    var filtered:[String] = []
    var searchString = [String]()
    var oldData = ["Shamim","Jaskirat","Danish", "Raghav","Pradeep"]
    var searchCounter = 0
    var userSelection = String()
    
    var imageCache = [String : UIImage]()
    var imageUrls = [String]()
    
    @IBAction func backButtonPressed(sender : AnyObject) {
    
    //self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
    super.viewDidLoad()
    let color = UIColor.whiteColor()
    var str = NSAttributedString(string: "Enter Text Here", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
    self.searchField.attributedPlaceholder = str;
   /// searchTable.registerClass(TableCell.classForCoder(), forCellReuseIdentifier: "cell")

    }

    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
    }
    
    // MARK : TextField Action
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
    if string != "" {                 //if element is typed not deleted
    searchString.append(string)
    var modifiedString = String()
    
    for i in searchString {
    modifiedString += i
    }
    searchCounter++
    println(modifiedString)
    println(searchString)
    search(modifiedString)
    } else {
    searchString.removeLast()
    var modifiedString = String()
    
    for i in searchString {
    modifiedString += i
    }
    searchCounter--
    println(searchString)
    println(modifiedString)
    search(modifiedString)
    
    }
    return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    self.searchField.endEditing(true)
    
    }
    
    // MARK: Search Implementation
    
    func search(string: String) {
    filtered = oldData.filter({ (text) -> Bool in
    let tmp: NSString = text
    let range = tmp.rangeOfString("\(string)", options: NSStringCompareOptions.CaseInsensitiveSearch)
    return range.location != NSNotFound
    })
    
    if(filtered == []) && searchCounter == 0 {
    data = oldData
    } else {
    data = filtered
    }
    
    sort(&filtered)
    println(filtered)
    searchTable.reloadData()
    
    }
    
    // MARK: Table Data
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filtered.count
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as TableStylistCell
    cell.stylistName.text = filtered[indexPath.row]
    return cell
    }
    
}
