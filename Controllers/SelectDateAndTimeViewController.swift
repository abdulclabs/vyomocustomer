//
//  SelectDateAndTimeViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/28/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SelectDateAndTimeViewController: UIViewController {
    
    //@IBOutlet weak var datePicker: UIDatePicker
    @IBOutlet weak var datePicker: UIDatePicker!
    let date = NSDate()
    let calendar = NSCalendar.currentCalendar()
//    let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date)
//    let hour = components.hour
//    let minutes = components.minute
    
    override func viewDidLoad() {
        super.viewDidLoad()
        printTimestamp()
//        println(hour)
//        dateLabel.text = "\(hour)"
        // Do any additional setup after loading the view.
    }

    func printTimestamp() {
        datePicker.datePickerMode = UIDatePickerMode.Date
      //  var day = datePicker.day
        let timestamp = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .MediumStyle, timeStyle: .ShortStyle)
        println(timestamp)
       // println(day)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
