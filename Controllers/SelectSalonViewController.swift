//
//  SelectSalonViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/28/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SelectSalonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var masterArray  =  Array(count: 3, repeatedValue: Array(count: 48, repeatedValue: 0))
    var timeStateone = [Int]()
    var timeStateTwo = [Int]()
    var timeStateThree = [Int]()
    var selectedTimeCounter = 0
    var startTime = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    timeStateone = Array(count: 48, repeatedValue: 0)
    timeStateTwo = Array(count: 48, repeatedValue: 0)
    timeStateThree = Array(count: 48, repeatedValue: 0)

        for(var i:Int=0;i<3;i++){
            for(var j:Int=0;j<48;j++){
                masterArray[i][j]=18   /*  Is this correct?  */
            }}
        
        for(var i:Int=0;i<3;i++){
            for(var j:Int=0;j<48;j++){
                println(masterArray[0])  /*  Is this correct?  */
            }
        }
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return 5
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as SelectSalonTableViewCell
        
//        let date = NSDate()
//        let cal = NSCalendar(calendarIdentifier: NSGregorianCalendar)
//        let components = cal.components(.CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: date)
//        let newDate = cal.dateFromComponents(components)//        let formatter = NSDateFormatter()
//        formatter.dateFormat = "HH"
//        let d = formatter.dateFromString("12")
//        println("formatted text is: \(d)")
            var  starButtonOriginX =  cell.scrollView.frame.origin.x
            var width: CGFloat = 60
            for var i = 0; i < 48; i++ {
                var  servicesButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 60))
                servicesButton.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
                servicesButton.tag = 2000 + 50*indexPath.row + i
                
                servicesButton.setTitle("11:30", forState: UIControlState.Normal)
                servicesButton.titleLabel!.font = (UIFont(name: "raleway", size: 16))
                servicesButton.setTitleColor(UIColor(red: 138/255, green: 138/255, blue: 138/255, alpha: 1), forState: .Normal)
                 servicesButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
                cell.scrollView.addSubview(servicesButton)
                 cell.scrollView.contentSize.width =  starButtonOriginX + width
                starButtonOriginX = starButtonOriginX + width
                
//                if timeStateone[i] == 1 {
//                    servicesButton.selected = true
//                    servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
//                }
//                if timeStateTwo[i] == 1 {
//                    servicesButton.selected = true
//                    servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
//                }
//                if timeStateThree[i] == 1 {
//                    servicesButton.selected = true
//                    servicesButton.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
//                }
            }
        
        
        return cell
    }
    
    func time(){
        startTime = startTime + 30
        
    }
    
    func pressed(sender: UIButton){
        println(sender.tag)
        if sender.selected {
            if sender.tag < 2050{
                timeStateone[sender.tag - 2000] = 0
                
            }else if sender.tag < 2100 && sender.tag >= 2050{
                timeStateTwo[sender.tag - 2050] = 0
            }
            else if sender.tag < 2150 && sender.tag >= 2100{
                timeStateThree[sender.tag - 2100] = 0
                
            }
//            else if sender.tag < 2200 && sender.tag >= 2150{
//                timeState[sender.tag - 2150] = 0
//                
//            }
//            else if sender.tag < 2250 && sender.tag >= 2200{
//                timeState[sender.tag - 2200] = 0
//                
//            }
//            else if sender.tag < 2300 && sender.tag >= 2250{
//                timeState[sender.tag - 2250] = 0
//               
//            }
//            else if sender.tag < 2350 && sender.tag >= 2300{
//                timeState[sender.tag - 2300] = 0
//                
//            }
//            else if sender.tag < 2400 && sender.tag >= 2350{
//                timeState[sender.tag - 2350] = 0
//                
//            }
//            else if sender.tag < 2450 && sender.tag >= 2400{
//                timeState[sender.tag - 2400] = 0
//            }
            selectedTimeCounter--
            sender.selected = false
            sender.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
            
        }
        else {
            if sender.tag < 2050{
                timeStateone[sender.tag - 2000] = 1
                
            }else if sender.tag < 2100 && sender.tag >= 2050{
                timeStateTwo[sender.tag - 2050] = 1
            }
            else if sender.tag < 2150 && sender.tag >= 2100{
                timeStateThree[sender.tag - 2100] = 1
                
            }
//            else if sender.tag < 2200 && sender.tag >= 2150{
//                timeState[sender.tag - 2150] = 1
//                
//            }
//            else if sender.tag < 2250 && sender.tag >= 2200{
//                timeState[sender.tag - 2200] = 1
//                
//            }
//            else if sender.tag < 2300 && sender.tag >= 2250{
//                timeState[sender.tag - 2250] = 1
//                
//            }
//            else if sender.tag < 2350 && sender.tag >= 2300{
//                timeState[sender.tag - 2300] = 1
//                
//            }
//            else if sender.tag < 2400 && sender.tag >= 2350{
//                timeState[sender.tag - 2350] = 1
//                
//            }
//            else if sender.tag < 2450 && sender.tag >= 2400{
//                timeState[sender.tag - 2400] = 1
//            }
            selectedTimeCounter++
            sender.selected = true
            sender.backgroundColor = UIColor(red: 149/255, green: 12/255, blue: 42/255, alpha: 1)
          //  let text = sender.titleLabel?.text
          //  selectedService.append(text!)
         //   println("HIIIii....")
          //  println(selectedService)
        }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
