//
//  StylistReviewViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/24/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class StylistReviewViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var stylistProfileImage: UIImageView!
    @IBOutlet weak var stylistName: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
   // @IBOutlet weak var distanceWithReview: NSLayoutConstraint!
    //@IBOutlet weak var distanceWithReview: NSLayoutConstraint!
    
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stylistProfileImage.layer.cornerRadius = self.stylistProfileImage.frame.size.width / 2;
        self.stylistProfileImage.clipsToBounds = true
        var tapRecognizer = UITapGestureRecognizer(target: self, action: "closeKeyBoard")
       //self.headerView.addGestureRecognizer(tapRecognizer)
       self.view.addGestureRecognizer(tapRecognizer)
        // Do any additional setup after loading the view.
    }
    
    func closeKeyBoard() {
        headerTopConstraint.constant = 0
        textViewBottomConstraint.constant = 10
        self.view.endEditing(true)
        println(textView.text)
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
       // scrollView.contentInset = contentInsets
        //scrollView.scrollIndicatorInsets = contentInsets
       //
        // If active text field is hidden by keyboard, scroll it so it's visible
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        let noteTextviewRect: CGRect? = textView.frame
        println(textView.frame)
        let noteTextviewOrigin: CGPoint? = noteTextviewRect?.origin
        if (!CGRectContainsPoint(aRect, noteTextviewOrigin!)) {
            //scrollView.scrollRectToVisible(noteTextviewRect!, animated:true)
        }
    }
    
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        //scrollView.contentInset = contentInsets
        //crollView.scrollIndicatorInsets = contentInsets
    }

    func textViewDidBeginEditing(textView: UITextView){
         headerTopConstraint.constant = -200
        textViewBottomConstraint.constant = 155
    }
    
    func textView(textView: UITextView!, shouldChangeTextInRange: NSRange, replacementText: NSString!) -> Bool {
        if(replacementText == "\n\n") {
             headerTopConstraint.constant = 0
            textViewBottomConstraint.constant = 10
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool{
        textView.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
