//
//  TestViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/16/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class TestViewController: UIViewController,UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var oldData = [1, 2, 3, 4, 5, 6]
    override func viewDidLoad() {
        super.viewDidLoad()
       scrollView.scrollEnabled = true;
        scrollView.contentSize = CGSizeMake(0   , 66);
        rating()
        // Do any additional setup after loading the view.
    }

    //rating....
    func scrollViewDidScroll(scrollView: UIScrollView) {
    
    }
    
    func  rating(){
        var  starButtonOriginX = scrollView.frame.origin.x
        var width: CGFloat = 80
        var n: CGFloat = CGFloat(oldData.count)
        for var i = 0; i < oldData.count; i++ {
            
            if(oldData.count <= 3)
            {
              var ratingButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
                ratingButton.backgroundColor = UIColor.yellowColor()
                ratingButton.tag = 610 + i
                //ratingButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
                ratingButton.setImage(UIImage(named: "unfilled@3x.png"), forState: UIControlState.Normal)
                scrollView.addSubview(ratingButton)
                scrollView.scrollEnabled = false
                scrollView.contentSize.width =  starButtonOriginX + width + 1
                starButtonOriginX = starButtonOriginX + width + 1
            }
            else
            {
            var ratingButton = UIButton(frame: CGRectMake(starButtonOriginX, 0, width, 66))
            ratingButton.backgroundColor = UIColor.yellowColor()
            ratingButton.tag = 610 + i
            //ratingButton.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            ratingButton.setImage(UIImage(named: "unfilled@3x.png"), forState: UIControlState.Normal)
            scrollView.addSubview(ratingButton)
            scrollView.contentSize.width += width
            starButtonOriginX = starButtonOriginX + width + 1
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
