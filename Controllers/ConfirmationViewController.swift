//
//  ConfirmationViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/24/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var viewSalonButton: UIButton!
    var servicesName = ["facial","wax","massage","hair cut"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.viewSalonButton.layer.cornerRadius = self.viewSalonButton.frame.size.height / 2
        self.viewSalonButton.layer.masksToBounds = true
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //tableHeight.constant = tableView.contentSize.height
        
        return servicesName.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as ConfirmationServicesTableViewCell
        cell.serviceLabel.text = servicesName[indexPath.row]
        return cell
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}
