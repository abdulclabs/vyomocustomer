//
//  MyAppointmentViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/22/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class MyAppointmentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var upcomingTableView: UITableView!
    @IBOutlet weak var completedTableView: UITableView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var poUpNotification: UIView!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var completedButton: UIButton!
    
    var services = ["Loreal", "Massage", "Facial", "Hair Cut"]
    
    @IBAction func upcomingButton(sender: UIButton) {
        upcomingTableView.hidden = false
        completedTableView.hidden = true
        headerViewHeight.constant = 70
         poUpNotification.hidden = true
        completedButton.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        upcomingButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
    }
    
    @IBAction func completedButton(sender: UIButton) {
        upcomingTableView.hidden = true
        completedTableView.hidden = false
        headerViewHeight.constant = 140
        poUpNotification.hidden = false
        completedButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        upcomingButton.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        upcomingTableView.rowHeight = 175
        completedTableView.rowHeight = 180
        upcomingTableView.hidden = false
        completedTableView.hidden = true
         poUpNotification.hidden = true
        // Do any additional setup after loading the view.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView.tag == 1001{
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as MyAppointmentTableOneViewCell
        cell.serviceNameLabel.text = services[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as MyAppointmentTableTwoViewCell
            return cell
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
