//
//  AppointmentDetailsViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/21/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class AppointmentDetailsViewController: UIViewController {
    
    @IBOutlet weak var rescheduleContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var rescheduleView: UIView!
    @IBOutlet weak var bookAgainButton: UIButton!
    
    @IBAction func emailButton(sender: AnyObject) {
        rescheduleView.hidden = false
        bookAgainButton.hidden = true
        rescheduleContainerHeight.constant = 140
    }
    @IBOutlet weak var whatsappButton: UIButton!
    @IBAction func whatsappButton(sender: UIButton) {
        
        bookAgainButton.hidden = false
        rescheduleView.hidden = true
        rescheduleContainerHeight.constant = 0
    }
   
    var serv = ["Hair Cut","Facial","Wax","Massage"]
    var cost = [100,150,230,400]
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookAgainButton.hidden = false
        rescheduleView.hidden = true
        rescheduleContainerHeight.constant = 0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //tableHeight.constant = tableView.contentSize.height
    return serv.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as StylistServicesTableViewCell
        
        cell.label.text = serv[indexPath.row]
        cell.stylistCostLabel.text = "\(cost[indexPath.row])"
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
