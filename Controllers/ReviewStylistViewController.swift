//
//  ReviewStylistViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ReviewStylistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var serviceTable: UITableView!
    var name = ["facial","wax","massage","hair cut"]
    var rupee = [100,125,150,75]
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLabel.shadowColor = UIColor.blackColor()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - table view implementation
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //tableHeight.constant = tableView.contentSize.height
        
        return name.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as ReviewStylistTableViewCell
         cell.titlelabel.text = name[indexPath.row]
        cell.rupeesCellLabel.text = "\(rupee[indexPath.row])"
        cell.deleteButton.addTarget(self, action: "deleteButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    
    func deleteButtonClicked(sender : UIButton) {
        var position: CGPoint = sender.convertPoint(CGPointZero, toView: self.serviceTable)
        if let indexPath = self.serviceTable.indexPathForRowAtPoint(position)
        {
            name.removeAtIndex(indexPath.row)
//            selectedBusinessServiceId.removeAtIndex(indexPath.row)
//            selectedIndexArray.removeAtIndex(indexPath.row)
            
            serviceTable.reloadData()
            tableHeight.constant = serviceTable.contentSize.height
        }
    }
   

}
