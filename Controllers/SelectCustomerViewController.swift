//
//  SelectCustomerController.swift
//  Vyomo
//
//  Created by Click Labs 65 on 3/12/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SelectCustomerViewController: UIViewController , UITextFieldDelegate ,UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchField: UITextField!
    var searchActive : Bool = false
    var data = [String]()
    var buisenessId = [Int]()
    var filtered:[String] = []
    var searchString = [String]()
    var oldData = [String]()
    var searchCounter = 0
    var userSelection = String()
    
    var imageCache = [String : UIImage]()
    var imageUrls = [String]()

    
    
    @IBAction func backButtonPressed(sender : AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let color = UIColor.whiteColor()
        var str = NSAttributedString(string: "Enter Text Here", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        self.searchField.attributedPlaceholder = str;
        searchTable.registerClass(TableCell.classForCoder(), forCellReuseIdentifier: "cell")
        
        
        
        
        var arr_parameters = NSDictionary(objectsAndKeys:
            "\(AccessToken)","access_token")
        json.session("get_customer_list", parameters: arr_parameters, completion: {
            result in
            
            let parsedResult = result as NSDictionary
            println(parsedResult)
            let resultArray = parsedResult["customer_list"] as NSArray
            println(resultArray)
            var resultCounter = 0
            
            for num in resultArray {
                let buisenessData = resultArray[resultCounter]["customer_full_name"] as String
                let buisenessIdData = resultArray[resultCounter]["customer_id"]!! as Int
                let buisness_image = resultArray[resultCounter]["customer_image_path"] as String
                self.imageUrls.append(buisness_image)
                self.data.append(buisenessData)
                self.buisenessId.append(buisenessIdData)
                resultCounter++
            }
            self.oldData = self.data
            dispatch_async(dispatch_get_main_queue()){
                self.searchTable.reloadData()
            }
        })
        
        
        
        
    }
    /*
    @IBAction func addNewButton(sender : AnyObject) {
        let othstoryboard = UIStoryboard(name: "SignupStoryboard", bundle: nil)
        let destinationView = othstoryboard.instantiateViewControllerWithIdentifier("addSaloonInfo") as AddSaloonInfoViewController
        let secondViewAnimation = Transition()
        secondViewAnimation.transitionForward(self, destViewController: destinationView)
        
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK : TextField Action
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string != "" {                 //if element is typed not deleted
            searchString.append(string)
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter++
            println(modifiedString)
            println(searchString)
            search(modifiedString)
        } else {
            searchString.removeLast()
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter--
            println(searchString)
            println(modifiedString)
            search(modifiedString)
            
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.searchField.endEditing(true)
        
    }
    
    // MARK: Search Implementation
    
    func search(string: String) {
        filtered = oldData.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString("\(string)", options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        if(filtered == []) && searchCounter == 0 {
            data = oldData
        } else {
            data = filtered
        }
        
        sort(&filtered)
        println(filtered)
        searchTable.reloadData()
        
    }
    
    // MARK: Table Data
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as TableCell
        cell.saloonNameLabel.text = "\(data[indexPath.row])"
        
        let urlString = "\(imageUrls[indexPath.row])"
        
        var image = self.imageCache[urlString]
        
        
        if( image == nil ) {
            // If the image does not exist, we need to download it
            var imgURL: NSURL = NSURL(string: urlString)!
            
            // Download an NSData representation of the image at the URL
            let request: NSURLRequest = NSURLRequest(URL: imgURL)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
                if error == nil {
                    image = UIImage(data: data)
                    
                    // Store the image in to our cache
                    self.imageCache[urlString] = image
                    dispatch_async(dispatch_get_main_queue(), {
                        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableCell {
                            cellToUpdate.saloonImage.image = image
                        }
                    })
                }
                else {
                    println("Error: \(error.localizedDescription)")
                }
            })
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), {
                if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableCell {
                    cellToUpdate.saloonImage.image = image
                }
            })
        }
        
        //getting images
        
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        userSelection = "\(data[indexPath.row])"
        println(userSelection)
        var serviceNumber = 0
        var selectedBuisnessUserId = 0
        while ( serviceNumber < oldData.count ) {
            if userSelection == oldData[serviceNumber] {
                selectedBuisnessUserId = serviceNumber
                break
            } else {
                serviceNumber++
            }
        }
        println(buisenessId[selectedBuisnessUserId])
        let n: Int! = self.navigationController?.viewControllers?.count
        let prevViewController = self.navigationController?.viewControllers[n-2] as AddAppointmentViewController
        prevViewController.customerName = userSelection
        prevViewController.customerId = buisenessId[selectedBuisnessUserId]
        prevViewController.customerButtonName = "Change"
        self.navigationController?.popViewControllerAnimated(true)

    }
}

